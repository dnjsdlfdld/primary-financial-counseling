# 자동차 금융 상담 API
***
#### LANGUAGE
```
* JAVA 18
* SpringBoot
* JPA
```
***
#### 기능
```
    - validation 패키지 활용
```
```
    1) 차량 데이터 관리
        
        [post]
        - 제조사 등록
        - 트림 등록
        - 차량 등급 등록
        - 차량 모델 등록
        [get]
        - 제조사 리스트 조회
        - 제조사 상세정보 조회
        - 트림 리스트 조회
        - 트림 상세정보 조회
        - 등급 리스트 조회
        - 등급 상세정보 조회
        - 모델 리스트 조회
        - 모델 상세정보 조회
        - 차량 정보 조회
        [put]
        - 제조사 정보수정
        - 트림 정보수정
        - 등급 정보수정
        - 모델 정보수정
    
    2) 상담내역 관리
        [post]
        - 상담내역 등록
        [put]
        - 트림 수정
        - 상담내역 수정(고객명, 연락처)
```
***
#### SWAGGER
> - 상담내역 관리
![consult_detail](./images/car1.png)

> - 차량 정보관리
![consult_detail](./images/car2.png)
![consult_detail](./images/car3.png)
![consult_detail](./images/car4.png)