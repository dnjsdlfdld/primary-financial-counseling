package com.jwi.primaryfinancialcounseling.interfaces;

// 할 일을 정해줌 구현은 니가 해라
public interface CommonModelBuilder<T> {
    T build(); //
}
