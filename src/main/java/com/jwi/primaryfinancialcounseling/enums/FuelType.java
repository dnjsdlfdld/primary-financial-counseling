package com.jwi.primaryfinancialcounseling.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FuelType {
    GASOLINE("가솔린"),
    DIESEL("경유"),
    HYBRID("하이브리드"),
    ELECTRIC("전기"),
    HYDROGEN("수소");

    private final String name;
}
