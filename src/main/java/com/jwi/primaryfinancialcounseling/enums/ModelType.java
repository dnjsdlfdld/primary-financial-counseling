package com.jwi.primaryfinancialcounseling.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ModelType {
    LIGHT_PUNISHMENT("경형"),
    SMALL_SIZE("소형"),
    MEDIUM_AND_MEDIUM("준중형"),
    MID_SIZE("중형"),
    SUBCOMPACT_VEHICLE("준대형"),
    LARGE("대형"),
    SPORTS("스포츠카");

    private final String name;
}
