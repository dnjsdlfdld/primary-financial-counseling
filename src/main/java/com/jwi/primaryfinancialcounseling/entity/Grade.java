package com.jwi.primaryfinancialcounseling.entity;

import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import com.jwi.primaryfinancialcounseling.model.grade.GradeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Grade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carModelId", nullable = false)
    private CarModel carModel;

    @Column(nullable = false, length = 50)
    private String gradeName;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putGradeId(CarModel carModel) {
        this.carModel = carModel;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putGrade(GradeRequest request) {
        this.gradeName = request.getGradeName();
        this.dateUpdate = LocalDateTime.now();
    }

    private Grade(GradeBuilder builder) {
        this.carModel = builder.carModel;
        this.gradeName = builder.gradeName;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class GradeBuilder implements CommonModelBuilder<Grade> {

        private final CarModel carModel;
        private final String gradeName;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public GradeBuilder(CarModel carModel, GradeRequest request) {
            this.carModel = carModel;
            this.gradeName = request.getGradeName();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
        @Override
        public Grade build() {
            return new Grade(this);
        }
    }
}
