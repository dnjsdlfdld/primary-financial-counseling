package com.jwi.primaryfinancialcounseling.entity;

import com.jwi.primaryfinancialcounseling.enums.FuelType;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import com.jwi.primaryfinancialcounseling.model.burp.BurpRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Burp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gradeId", nullable = false)
    private Grade grade;
    @Column(nullable = false, length = 30)
    private String burpName;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Integer displacement;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 50)
    private FuelType fuelType;
    @Column(nullable = false)
    private Integer highestOutput;
    @Column(nullable = false)
    private Double maximumTorque;
    @Column(nullable = false, length = 10)
    private String driveMethod;
    @Column(nullable = false)
    private Double combinedFuelEconomy;
    @Column(nullable = false)
    private Double cityFuelEfficiency;
    @Column(nullable = false)
    private Double highwayFuelEfficiency;
    @Column(nullable = false)
    private Integer fuelEconomyRating;
    @Column(nullable = false)
    private Double toleranceWeight;
    @Column(nullable = false, length = 20)
    private String frontTireSpecification;
    @Column(nullable = false, length = 20)
    private String rearTireSpecification;
    @Column(nullable = false)
    private Integer lowPollutionGrade;
    @Column(nullable = false)
    private Integer topSpeed;
    @Column(nullable = false, length = 10)
    private Integer fixedNumber;
    @Column(nullable = false)
    private Double carbonDioxideEmissions;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putBurpId(Grade grade) {
        this.grade = grade;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putBurp(BurpRequest request) {
        this.burpName = request.getBurpName();
        this.price = request.getPrice();
        this.displacement = request.getDisplacement();
        this.fuelType = request.getFuelType();
        this.highestOutput = request.getHighestOutput();
        this.maximumTorque = request.getMaximumTorque();
        this.driveMethod = request.getDriveMethod();
        this.combinedFuelEconomy = request.getCombinedFuelEconomy();
        this.cityFuelEfficiency = request.getCityFuelEfficiency();
        this.highwayFuelEfficiency = request.getHighwayFuelEfficiency();
        this.fuelEconomyRating = request.getFuelEconomyRating();
        this.toleranceWeight = request.getToleranceWeight();
        this.frontTireSpecification = request.getFrontTireSpecification();
        this.rearTireSpecification = request.getRearTireSpecification();
        this.lowPollutionGrade = request.getLowPollutionGrade();
        this.topSpeed = request.getTopSpeed();
        this.fixedNumber = request.getFixedNumber();
        this.carbonDioxideEmissions = request.getCarbonDioxideEmissions();
        this.dateUpdate = LocalDateTime.now();
    }

    private Burp(BurpBuilder builder) {
        this.grade = builder.grade;
        this.burpName = builder.burpName;
        this.price = builder.price;
        this.displacement = builder.displacement;
        this.fuelType = builder.fuelType;
        this.highestOutput = builder.highestOutput;
        this.maximumTorque = builder.maximumTorque;
        this.driveMethod = builder.driveMethod;
        this.combinedFuelEconomy = builder.combinedFuelEconomy;
        this.cityFuelEfficiency = builder.cityFuelEfficiency;
        this.highwayFuelEfficiency = builder.highwayFuelEfficiency;
        this.fuelEconomyRating = builder.fuelEconomyRating;
        this.toleranceWeight = builder.toleranceWeight;
        this.frontTireSpecification = builder.frontTireSpecification;
        this.rearTireSpecification = builder.rearTireSpecification;
        this.lowPollutionGrade = builder.lowPollutionGrade;
        this.topSpeed = builder.topSpeed;
        this.fixedNumber = builder.fixedNumber;
        this.carbonDioxideEmissions = builder.carbonDioxideEmissions;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class BurpBuilder implements CommonModelBuilder<Burp> {
        private final Grade grade;
        private final String burpName;
        private final Double price;
        private final Integer displacement;
        private final FuelType fuelType;
        private final Integer highestOutput;
        private final Double maximumTorque;
        private final String driveMethod;
        private final Double combinedFuelEconomy;
        private final Double cityFuelEfficiency;
        private final Double highwayFuelEfficiency;
        private final Integer fuelEconomyRating;
        private final Double toleranceWeight;
        private final String frontTireSpecification;
        private final String rearTireSpecification;
        private final Integer lowPollutionGrade;
        private final Integer topSpeed;
        private final Integer fixedNumber;
        private final Double carbonDioxideEmissions;

        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public BurpBuilder(Grade grade, BurpRequest request) {
            this.grade = grade;
            this.burpName = request.getBurpName();
            this.price = request.getPrice();
            this.displacement = request.getDisplacement();
            this.fuelType = request.getFuelType();
            this.highestOutput = request.getHighestOutput();
            this.maximumTorque = request.getMaximumTorque();
            this.driveMethod = request.getDriveMethod();
            this.combinedFuelEconomy = request.getCombinedFuelEconomy();
            this.cityFuelEfficiency = request.getCityFuelEfficiency();
            this.highwayFuelEfficiency = request.getHighwayFuelEfficiency();
            this.fuelEconomyRating = request.getFuelEconomyRating();
            this.toleranceWeight = request.getToleranceWeight();
            this.frontTireSpecification = request.getFrontTireSpecification();
            this.rearTireSpecification = request.getRearTireSpecification();
            this.lowPollutionGrade = request.getLowPollutionGrade();
            this.topSpeed = request.getTopSpeed();
            this.fixedNumber = request.getFixedNumber();
            this.carbonDioxideEmissions = request.getCarbonDioxideEmissions();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Burp build() {
            return new Burp(this);
        }
    }
}
