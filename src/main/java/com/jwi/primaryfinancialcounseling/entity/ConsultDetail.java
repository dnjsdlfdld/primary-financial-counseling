package com.jwi.primaryfinancialcounseling.entity;

import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import com.jwi.primaryfinancialcounseling.model.consultdetail.ConsultDetailRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "burpId", nullable = false)
    private Burp burp;

    @Column(nullable = false, length = 20)
    private String customName;

    @Column(nullable = false, length = 13)
    private String phone;

    @Column(nullable = false)
    private LocalDateTime applicationTime;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putConsultDetailId(Burp burp) {
        this.burp = burp;
    }

    public void putConsultDetail(ConsultDetailRequest request) {
        this.customName = request.getCustomName();
        this.phone = request.getPhone();
        this.dateUpdate = LocalDateTime.now();
    }

    private ConsultDetail(ConsultDetailBuilder builder) {
        this.burp = builder.burp;
        this.customName = builder.customName;
        this.phone = builder.phone;
        this.applicationTime = builder.applicationTime;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ConsultDetailBuilder implements CommonModelBuilder<ConsultDetail> {

        private final Burp burp;
        private final String customName;
        private final String phone;
        private final LocalDateTime applicationTime;
        private final LocalDateTime dateUpdate;

        public ConsultDetailBuilder(Burp burp, ConsultDetailRequest request) {
            this.burp = burp;
            this.customName = request.getCustomName();
            this.phone = request.getPhone();
            this.applicationTime = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public ConsultDetail build() {
            return new ConsultDetail(this);
        }
    }
}
