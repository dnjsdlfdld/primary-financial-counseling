package com.jwi.primaryfinancialcounseling.entity;

import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import com.jwi.primaryfinancialcounseling.model.carbrand.CarBrandRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarBrand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String brandName;

    @Column(nullable = false, length = 150)
    private String brandImage;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putBrand(CarBrandRequest request) {
        this.brandName = request.getBrandName();
        this.brandImage =request.getBrandImage();
        this.dateUpdate = LocalDateTime.now();
    }

    private CarBrand(CarBrandBuilder builder) {
        this.brandName = builder.brandName;
        this.brandImage = builder.brandImage;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarBrandBuilder implements CommonModelBuilder<CarBrand> {
        private final String brandName;
        private final String brandImage;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        public CarBrandBuilder(CarBrandRequest request) {
            this.brandName = request.getBrandName();
            this.brandImage = request.getBrandImage();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarBrand build() {
            return new CarBrand(this);
        }
    }
}
