package com.jwi.primaryfinancialcounseling.entity;

import com.jwi.primaryfinancialcounseling.enums.ModelType;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import com.jwi.primaryfinancialcounseling.model.carmodel.CarModelRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carBrandId", nullable = false)
    private CarBrand carBrand;

    @Column(nullable = false, length = 30)
    private String modelName;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 18)
    private ModelType modelType;

    @Column(nullable = false, length = 20)
    private String external;

    @Column(nullable = false)
    private Integer modelYear;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putCarModelId(CarBrand carBrand) {
        this.carBrand = carBrand;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putCarModel(CarModelRequest request) {
        this.modelName = request.getModelName();
        this.modelType = request.getModelType();
        this.external = request.getExternal();
        this.modelYear = request.getModelYear();
        this.dateUpdate = LocalDateTime.now();
    }

    private CarModel(CarModelBuilder builder) {
        this.carBrand = builder.carBrand;
        this.modelName = builder.modelName;
        this.modelType = builder.modelType;
        this.external = builder.external;
        this.modelYear = builder.modelYear;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarModelBuilder implements CommonModelBuilder<CarModel> {

        private final CarBrand carBrand;
        private final String modelName;
        private final ModelType modelType;
        private final String external;
        private final Integer modelYear;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarModelBuilder(CarBrand carBrand, CarModelRequest request) {
            this.carBrand = carBrand;
            this.modelName = request.getModelName();
            this.modelType = request.getModelType();
            this.external = request.getExternal();
            this.modelYear = request.getModelYear();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarModel build() {
            return new CarModel(this);
        }
    }
}
