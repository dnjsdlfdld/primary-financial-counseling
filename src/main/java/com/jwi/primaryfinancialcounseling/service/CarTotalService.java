package com.jwi.primaryfinancialcounseling.service;

import com.jwi.primaryfinancialcounseling.entity.Burp;
import com.jwi.primaryfinancialcounseling.entity.CarBrand;
import com.jwi.primaryfinancialcounseling.entity.CarModel;
import com.jwi.primaryfinancialcounseling.entity.Grade;
import com.jwi.primaryfinancialcounseling.exception.CMissingDataException;
import com.jwi.primaryfinancialcounseling.model.CarInfoDetailResponse;
import com.jwi.primaryfinancialcounseling.model.CarListItem;
import com.jwi.primaryfinancialcounseling.model.burp.BurpItem;
import com.jwi.primaryfinancialcounseling.model.burp.BurpRequest;
import com.jwi.primaryfinancialcounseling.model.carbrand.CarBrandItem;
import com.jwi.primaryfinancialcounseling.model.carbrand.CarBrandRequest;
import com.jwi.primaryfinancialcounseling.model.carmodel.CarModelItem;
import com.jwi.primaryfinancialcounseling.model.carmodel.CarModelRequest;
import com.jwi.primaryfinancialcounseling.model.etc.ListResult;
import com.jwi.primaryfinancialcounseling.model.grade.GradeItem;
import com.jwi.primaryfinancialcounseling.model.grade.GradeRequest;
import com.jwi.primaryfinancialcounseling.repository.BurpRepository;
import com.jwi.primaryfinancialcounseling.repository.CarBrandRepository;
import com.jwi.primaryfinancialcounseling.repository.CarModelRepository;
import com.jwi.primaryfinancialcounseling.repository.GradeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class CarTotalService {
    private final CarBrandRepository carBrandRepository;
    private final CarModelRepository carModelRepository;
    private final GradeRepository gradeRepository;
    private final BurpRepository burpRepository;

    public void setCarBrand(CarBrandRequest request) {
        CarBrand carBrand = new CarBrand.CarBrandBuilder(request).build();
        carBrandRepository.save(carBrand);
    }

    public CarBrandItem getCarBrand(long carBrandId) {
        CarBrand carBrand = carBrandRepository.findById(carBrandId).orElseThrow(CMissingDataException::new);

        return new CarBrandItem.CarBrandItemBuilder(carBrand).build();
    }

    public ListResult<CarBrandItem> getCarBrands() {
        List<CarBrand> carBrands = carBrandRepository.findAll();
        List<CarBrandItem> result = new LinkedList<>();

        carBrands.forEach(carBrand -> {
            CarBrandItem carBrandItem = new CarBrandItem.CarBrandItemBuilder(carBrand).build();
            result.add(carBrandItem);
        });

        return ListConvertService.settingResult(result);
    }

    public CarBrand getCarBrandId(long carBrandId) {
        return carBrandRepository.findById(carBrandId).orElseThrow(CMissingDataException::new);
    }

    public void putCarBrand(long carBrandId, CarBrandRequest request) {
        CarBrand carBrand = carBrandRepository.findById(carBrandId).orElseThrow(CMissingDataException::new);
        carBrand.putBrand(request);
        carBrandRepository.save(carBrand);
    }

    public void setCarModel(CarBrand carBrand, CarModelRequest request) {
        CarModel carModel = new CarModel.CarModelBuilder(carBrand, request).build();
        carModelRepository.save(carModel);
    }

    public CarModelItem getCarModel(long carModelId) {
        CarModel carModel = carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);

        return new CarModelItem.CarModelItemBuilder(carModel).build();
    }

    public ListResult<CarModelItem> getCarModels() {
        List<CarModel> carModels = carModelRepository.findAll();
        List<CarModelItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarModelItem carModelItem = new CarModelItem.CarModelItemBuilder(carModel).build();
            result.add(carModelItem);
        });

        return ListConvertService.settingResult(result);
    }

    public CarModel getCarModelId(long carModelId) {
        return carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);
    }

    public ListResult<CarListItem> getCarModelNews() {
        List<CarModel> carModels = carModelRepository.findAll();
        List<CarListItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarListItem carListItem = new CarListItem.CarListItemBuilder(carModel).build();
            result.add(carListItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putCarModel(long carModelId, CarModelRequest request) {
        CarModel carModel = carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);
        carModel.putCarModel(request);
        carModelRepository.save(carModel);
    }

    public void putCarModelId(long carModelId, CarBrand carBrand) {
        CarModel carModel = carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);
        carModel.putCarModelId(carBrand);
        carModelRepository.save(carModel);
    }

    public void setGrade(CarModel carModel, GradeRequest request) {
        Grade grade = new Grade.GradeBuilder(carModel, request).build();
        gradeRepository.save(grade);
    }

    public GradeItem getGrade(long gradeId) {
        Grade grade = gradeRepository.findById(gradeId).orElseThrow(CMissingDataException::new);

        return new GradeItem.GradeItemBuilder(grade).build();
    }

    public ListResult<GradeItem> getGrades() {
        List<Grade> grades = gradeRepository.findAll();
        List<GradeItem> result = new LinkedList<>();

        grades.forEach(grade -> {
            GradeItem gradeItem = new GradeItem.GradeItemBuilder(grade).build();
            result.add(gradeItem);
        });

        return ListConvertService.settingResult(result);
    }

    public Grade getGradeId(long gradeId) {
        return gradeRepository.findById(gradeId).orElseThrow(CMissingDataException::new);
    }

    public void putGrade(long gradeId, GradeRequest request) {
        Grade grade = gradeRepository.findById(gradeId).orElseThrow(CMissingDataException::new);
        grade.putGrade(request);
        gradeRepository.save(grade);
    }

    public void putGradeId(long gradeId, CarModel carModel) {
        Grade grade = gradeRepository.findById(gradeId).orElseThrow(CMissingDataException::new);
        grade.putGradeId(carModel);
        gradeRepository.save(grade);
    }

    public void setBurp(Grade grade, BurpRequest request) {
        Burp burp = new Burp.BurpBuilder(grade, request).build();
        burpRepository.save(burp);
    }

    public BurpItem getBurp(long burpId) {
        Burp burp = burpRepository.findById(burpId).orElseThrow(CMissingDataException::new);
        BurpItem result = new BurpItem.BurpItemBuilder(burp).build();

        return result;
    }

    public ListResult<BurpItem> getBurps() {
        List<Burp> burps = burpRepository.findAll();
        List<BurpItem> result = new LinkedList<>();

        burps.forEach(burp -> {
            BurpItem burpItem = new BurpItem.BurpItemBuilder(burp).build();
            result.add(burpItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putBurp(long burpId, BurpRequest request) {
        Burp burp = burpRepository.findById(burpId).orElseThrow(CMissingDataException::new);
        burp.putBurp(request);
        burpRepository.save(burp);
    }

    public void putBurpId(long burpId, Grade grade) {
        Burp burp = burpRepository.findById(burpId).orElseThrow(CMissingDataException::new);
        burp.putBurpId(grade);
        burpRepository.save(burp);
    }

    public Burp getBurpId(long burpId) {
        return burpRepository.findById(burpId).orElseThrow(CMissingDataException::new);
    }

    public CarInfoDetailResponse getDetail(long carModelId) {
        CarModel carModel = carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);
        // 처음에 모델의 원본을 가지고 옴.
        List<Burp> burps = burpRepository.findAllByGrade_CarModel_Id(carModel.getId());
        // 트림들을 가지고 와야 하기 때문에 트림레퍼지토리에 쿼리작성 다가져와 등급으로 올라가서 모델로 올라가고 거기에 Id와 일치하는 데이터를
        // 불러와
        ArrayList<Double> prices = new ArrayList<>();

        burps.forEach(burp -> {
            prices.add(burp.getPrice());
        });


        prices.sort(Collections.reverseOrder());

        Double maxPrice = prices.get(0);
        Double minPrice = prices.get(prices.size() - 1); // 5개씩 보여주는데 총 몇 개인지 알 수 없으니 size 를 넣는다. 0부터 시작하니 -1
        // 마지막의 index 값을 가져오게 된다.
        /**
         * Double만 들어가는 배열을 만든다.
         * 정렬시키고 0번째 데이터를 가져오고 배열의 마지막 4번째 데이터를 가져온다.
         *
         */
        List<String> carFuelTypes = new LinkedList<>();
        burps.forEach(burp -> {
            carFuelTypes.add(burp.getFuelType().getName()); // 가솔린,가솔린,디젤,LPG 등등 리스트가 중복되지만 일단 모았음
        }); // List 에서 Set 으로 변환
        Set<String> tempSet = new HashSet<>(carFuelTypes); // 중복값을 제거해주기 위해
        List<String> carFuelTypesResult = new ArrayList<>(tempSet);
        /**
         * List일 경우 중복값이 생기기 때문에 Set을 통하여 중복값 제거 후 List값으로 주기로 했으니 다시 List로 전환
         */
        ArrayList<Integer> displacement = new ArrayList<>();
        burps.forEach(burp -> {
            displacement.add(burp.getDisplacement());
        });

        displacement.sort(Collections.reverseOrder());

        Integer maxDisplacement = displacement.get(0);
        Integer minDisplacement = displacement.get(displacement.size() -1);

        ArrayList<Double> fuelEfficiency = new ArrayList<>();
        burps.forEach(burp -> {
            fuelEfficiency.add(burp.getCombinedFuelEconomy());
        });

        fuelEfficiency.sort(Collections.reverseOrder());

        Double maxFuelEfficiency = fuelEfficiency.get(0);
        Double minFuelEfficiency = fuelEfficiency.get(fuelEfficiency.size() -1);

        ArrayList<Integer> personnelCount = new ArrayList<>();
        burps.forEach(burp -> {
            personnelCount.add(burp.getFixedNumber());
        });

        personnelCount.sort(Collections.reverseOrder());

        Integer maxPersonnelCount = personnelCount.get(0);
        Integer minPersonnelCount = personnelCount.get(personnelCount.size() -1);

        return new CarInfoDetailResponse.CarInfoDetailResponseBuilder(
                carModel,
                minPrice,
                maxPrice,
                carFuelTypesResult,
                minDisplacement,
                maxDisplacement,
                minFuelEfficiency,
                maxFuelEfficiency,
                minPersonnelCount,
                maxPersonnelCount

        ).build();


    }
}
