package com.jwi.primaryfinancialcounseling.service;

import com.jwi.primaryfinancialcounseling.model.etc.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService { // ListResult 의 3개를 구현하기 위한 것
    public static PageRequest getPageable(int pageNum) {
        // 페이지를 나눌 수 있는 메소드
        return PageRequest.of(pageNum - 1, 10);
    }

    public static PageRequest getPageable(int pageNum, int pageSize) {
        // 위와 같은 이름의 메소드지만 각자 일하는게 차이가 있어서 사용이 가능
        return PageRequest.of(pageNum - 1, pageSize);
        // 위 메소드는 기본으로 10개를 표현해주지만, 이 메소드는 유동적
    }

    public static <T> ListResult<T> settingResult(List<T> list) { // 어떤 형태의 리스트(뭉탱이전체)를 받았다
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount((long)list.size());
        result.setTotalPage(1); // 전체 페이지 1
        result.setCurrentPage(1); // 현재 페이지 1  1/1

        return result;
    }

    public static <T> ListResult<T> settingResult(List<T> list, long totalItemCount, int totalPage, int currentPage) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount(totalItemCount);
        result.setTotalPage(totalPage == 0 ? 1 : totalPage); // 페이지가 0페이지 부터 시작하기 떄문에 0이라고 표현해줄 수 없으니 1로
        result.setCurrentPage(currentPage + 1); // 그 이후 페이지에서는 계속 1을 더하여 표현

        return result;
    }
}
