package com.jwi.primaryfinancialcounseling.service;

import com.jwi.primaryfinancialcounseling.entity.Burp;
import com.jwi.primaryfinancialcounseling.entity.ConsultDetail;
import com.jwi.primaryfinancialcounseling.exception.CMissingDataException;
import com.jwi.primaryfinancialcounseling.model.consultdetail.ConsultDetailRequest;
import com.jwi.primaryfinancialcounseling.repository.ConsultDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConsultDetailService {
    private final ConsultDetailRepository consultDetailRepository;

    public void setConsultDetail(Burp burp, ConsultDetailRequest request) {
        ConsultDetail consultDetail = new ConsultDetail.ConsultDetailBuilder(burp, request).build();
        consultDetailRepository.save(consultDetail);
    }


    public void putConsultDetail(long consultDetailId, ConsultDetailRequest request) {
        ConsultDetail consultDetail = consultDetailRepository.findById(consultDetailId).orElseThrow(CMissingDataException::new);
        consultDetail.putConsultDetail(request);
        consultDetailRepository.save(consultDetail);
    }

    public void putConsultDetailId(long consultDetailId, Burp burp) {
        ConsultDetail consultDetail = consultDetailRepository.findById(consultDetailId).orElseThrow(CMissingDataException::new);
        consultDetail.putConsultDetailId(burp);
        consultDetailRepository.save(consultDetail);
    }
}
