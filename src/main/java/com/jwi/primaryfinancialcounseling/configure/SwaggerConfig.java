package com.jwi.primaryfinancialcounseling.configure;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;

@Configuration // 설정 파일이기 때문에
@EnableSwagger2 // Swagger2 를 사용 가능하게 하다
public class SwaggerConfig {
    private String version;
    private String title;
    private final String TITLE_FIX = "자동차 금융 상담(1차) ";
    // final 변하지 않겠다. TITLE_FIX > 상수

    @Bean
    public Docket apiV1() {
        // Springfox 에서 제공해주는 타입
        version = "V1";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.jwi.primaryfinancialcounseling"))
                .paths(PathSelectors.ant("/v1/**"))
                .build()
                .apiInfo(getApiInfo(title, version))
                .securitySchemes(Collections.singletonList(getApiKey()))
                .enable(true);
    }

    @Bean
    public Docket apiV2() {
        // Springfox 에서 제공해주는 타입
        version = "V2";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.jwi.primaryfinancialcounseling"))
                .paths(PathSelectors.ant("/v2/**"))
                .build()
                .apiInfo(getApiInfo(title, version))
                .securitySchemes(Collections.singletonList(getApiKey()))
                .enable(true);
    }

    private ApiInfo getApiInfo(String title, String version) {
        return new ApiInfo(
                title,
                "Swagger API Docs",
                version,
                "JoWonIl.com",
                new Contact("jwi", "JoWonIl.com", "dnjsdlfdleka1@naver.com"),
                "Licenses",
                "JoWonIl.com",
                new ArrayList<>()
        );
    }

    private ApiKey getApiKey() {
        return new ApiKey("jwtToken", "X-AUTH-TOKEN", "header");
    }
}
