package com.jwi.primaryfinancialcounseling.model.carbrand;

import com.jwi.primaryfinancialcounseling.entity.CarBrand;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarBrandItem {
    @ApiModelProperty(notes = "제조사 등록번호")
    private Long id;

    @ApiModelProperty(notes = "제조사명")
    private String brandName;

    @ApiModelProperty(notes = "제조사 로고")
    private String brandImage;

    private CarBrandItem(CarBrandItemBuilder builder) {
        this.id = builder.id;
        this.brandName = builder.brandName;
        this.brandImage = builder.brandImage;
    }

    public static class CarBrandItemBuilder implements CommonModelBuilder<CarBrandItem> {
        private final Long id;
        private final String brandName;
        private final String brandImage;

        public CarBrandItemBuilder(CarBrand carBrand) {
            this.id = carBrand.getId();
            this.brandName = carBrand.getBrandName();
            this.brandImage = carBrand.getBrandImage();
        }

        @Override
        public CarBrandItem build() {
            return new CarBrandItem(this);
        }
    }
}
