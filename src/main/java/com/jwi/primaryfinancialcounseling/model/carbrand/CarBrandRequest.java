package com.jwi.primaryfinancialcounseling.model.carbrand;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarBrandRequest {
    @NotNull
    @Length(min = 2, max = 30)
    @ApiModelProperty(notes = "제조사명", required = true)
    private String brandName;

    @NotNull
    @Length(min = 1, max = 150)
    @ApiModelProperty(notes = "제조사 로고", required = true)
    private String brandImage;
}
