package com.jwi.primaryfinancialcounseling.model.grade;

import com.jwi.primaryfinancialcounseling.entity.Grade;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GradeItem {

    @ApiModelProperty(notes = "제조사명")
    private String brandName;

    @ApiModelProperty(notes = "제조사 로고")
    private String brandImage;

    @ApiModelProperty(notes = "모델 등록번호")
    private Long carModelId;

    @ApiModelProperty(notes = "모델명")
    private String modelFullName;

    @ApiModelProperty(notes = "외장")
    private String external;

    @ApiModelProperty(notes = "년식")
    private Integer modelYear;

    @ApiModelProperty(notes = "등급명")
    private String gradeName;

    private GradeItem(GradeItemBuilder builder) {
        this.brandName = builder.brandName;
        this.brandImage = builder.brandImage;
        this.carModelId = builder.carModelId;
        this.modelFullName = builder.modelFullName;
        this.external = builder.external;
        this.modelYear = builder.modelYear;
        this.gradeName = builder.gradeName;
    }

    public static class GradeItemBuilder implements CommonModelBuilder<GradeItem> {


        private final String brandName;
        private final String brandImage;
        private final Long carModelId;
        private final String modelFullName;
        private final String external;
        private final Integer modelYear;
        private final String gradeName;

        public GradeItemBuilder(Grade grade) {
            this.brandName = grade.getCarModel().getCarBrand().getBrandName();
            this.brandImage = grade.getCarModel().getCarBrand().getBrandImage();
            this.carModelId = grade.getCarModel().getId();
            this.modelFullName = grade.getCarModel().getModelName() + " [" + grade.getCarModel().getModelType().getName() + "]";
            this.external = grade.getCarModel().getExternal();
            this.modelYear = grade.getCarModel().getModelYear();
            this.gradeName = grade.getGradeName();
        }

        @Override
        public GradeItem build() {
            return new GradeItem(this);
        }
    }
}
