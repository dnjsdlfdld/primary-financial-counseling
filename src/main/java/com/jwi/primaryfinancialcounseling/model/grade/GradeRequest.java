package com.jwi.primaryfinancialcounseling.model.grade;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GradeRequest {
    @NotNull
    @ApiModelProperty(notes = "모델 등록번호", required = true)
    private Long carModelId;

    @NotNull
    @Length(min = 2, max = 50)
    @ApiModelProperty(notes = "등급명", required = true)
    private String gradeName;
}
