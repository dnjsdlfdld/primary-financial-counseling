package com.jwi.primaryfinancialcounseling.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarInfoDetailRangeItem { // Response 에서 빌더 구현할 것이고 여기서는 Setter를 쓰겠다.
    private Integer minIntValue;

    private Integer maxIntValue;

    private Double minDoubleValue;

    private Double maxDoubleValue;

}
