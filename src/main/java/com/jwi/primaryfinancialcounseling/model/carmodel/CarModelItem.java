package com.jwi.primaryfinancialcounseling.model.carmodel;

import com.jwi.primaryfinancialcounseling.entity.CarModel;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelItem {
    @ApiModelProperty(notes = "제조사명")
    private String brandName;

    @ApiModelProperty(notes = "제조사 로고")
    private String brandImage;

    @ApiModelProperty(notes = "모델 등록번호")
    private Long carModelId;

    @ApiModelProperty(notes = "모델명")
    private String modelFullName;

    @ApiModelProperty(notes = "외장")
    private String external;

    @ApiModelProperty(notes = "년식")
    private String modelYear;

    private CarModelItem(CarModelItemBuilder builder) {
        this.brandName = builder.brandName;
        this.brandImage = builder.brandImage;
        this.carModelId = builder.carModelId;
        this.modelFullName = builder.modelFullName;
        this.external = builder.external;
        this.modelYear = builder.modelYear;
    }

    public static class CarModelItemBuilder implements CommonModelBuilder<CarModelItem> {

        private final String brandName;
        private final String brandImage;
        private final Long carModelId;
        private final String modelFullName;
        private final String external;
        private final String modelYear;

        public CarModelItemBuilder(CarModel carModel) {
            this.brandName = carModel.getCarBrand().getBrandName();
            this.brandImage = carModel.getCarBrand().getBrandImage();
            this.carModelId = carModel.getId();
            this.modelFullName = carModel.getModelName() + " [" + carModel.getModelType().getName() + "]";
            this.external = carModel.getExternal();
            this.modelYear = carModel.getModelYear() + "년형";
        }

        @Override
        public CarModelItem build() {
            return new CarModelItem(this);
        }
    }
}
