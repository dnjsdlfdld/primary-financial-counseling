package com.jwi.primaryfinancialcounseling.model.carmodel;

import com.jwi.primaryfinancialcounseling.enums.ModelType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelRequest {
    @NotNull
    @ApiModelProperty(notes = "제조사 등록번호", required = true)
    private Long carBrandId;

    @NotNull
    @Length(min = 2, max = 30)
    @ApiModelProperty(notes = "모델명", required = true)
    private String modelName;

    @NotNull
    @ApiModelProperty(notes = "모델 유형", required = true)
    private ModelType modelType;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "외장", required = true)
    private String external;

    @NotNull
    @ApiModelProperty(notes = "차량 년식", required = true)
    private Integer modelYear;
}
