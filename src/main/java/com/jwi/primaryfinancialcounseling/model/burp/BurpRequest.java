package com.jwi.primaryfinancialcounseling.model.burp;

import com.jwi.primaryfinancialcounseling.entity.Grade;
import com.jwi.primaryfinancialcounseling.enums.FuelType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BurpRequest {
    @NotNull
    @ApiModelProperty(notes = "등급 등록번호", required = true)
    private Long gradeId;
    @NotNull
    @Length(min = 1, max = 30)
    @ApiModelProperty(notes = "트림명", required = true)
    private String burpName;
    @NotNull
    @ApiModelProperty(notes = "가격", required = true)
    private Double price;
    @NotNull
    @ApiModelProperty(notes = "배기량", required = true)
    private Integer displacement;
    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "연료", required = true)
    private FuelType fuelType;
    @NotNull
    @ApiModelProperty(notes = "최고출력", required = true)
    private Integer highestOutput;
    @NotNull
    @ApiModelProperty(notes = "최대토크", required = true)
    private Double maximumTorque;
    @NotNull
    @Length(min = 1, max = 10)
    @ApiModelProperty(notes = "구동방식", required = true)
    private String driveMethod;
    @NotNull
    @ApiModelProperty(notes = "복합연비", required = true)
    private Double combinedFuelEconomy;
    @NotNull
    @ApiModelProperty(notes = "도심연비", required = true)
    private Double cityFuelEfficiency;
    @NotNull
    @ApiModelProperty(notes = "고속도로연비", required = true)
    private Double highwayFuelEfficiency;
    @NotNull
    @ApiModelProperty(notes = "연비(등급)", required = true)
    private Integer fuelEconomyRating;
    @NotNull
    @ApiModelProperty(notes = "공차중량", required = true)
    private Double toleranceWeight;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "앞타이어규격", required = true)
    private String frontTireSpecification;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "뒷타이어규격", required = true)
    private String rearTireSpecification;
    @NotNull
    @ApiModelProperty(notes = "저공해등급", required = true)
    private Integer lowPollutionGrade;
    @NotNull
    @ApiModelProperty(notes = "최고속도", required = true)
    private Integer topSpeed;
    @NotNull
    @ApiModelProperty(notes = "정원", required = true)
    private Integer fixedNumber;
    @NotNull
    @ApiModelProperty(notes = "이산화탄소배출량", required = true)
    private Double carbonDioxideEmissions;

}
