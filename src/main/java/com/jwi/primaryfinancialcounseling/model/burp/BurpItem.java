package com.jwi.primaryfinancialcounseling.model.burp;

import com.jwi.primaryfinancialcounseling.entity.Burp;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BurpItem {
    @ApiModelProperty(notes = "제조사명")
    private String brandName;

    @ApiModelProperty(notes = "제조사 로고")
    private String brandImage;

    @ApiModelProperty(notes = "모델 등록번호")
    private Long carModelId;

    @ApiModelProperty(notes = "모델명")
    private String modelFullName;

    @ApiModelProperty(notes = "외장")
    private String external;

    @ApiModelProperty(notes = "년식")
    private Integer modelYear;

    @ApiModelProperty(notes = "등급명")
    private String gradeName;

    @ApiModelProperty(notes = "트림명")
    private String burpName;

    @ApiModelProperty(notes = "가격")
    private String price;

    @ApiModelProperty(notes = "배기량")
    private String displacement;

    @ApiModelProperty(notes = "연료")
    private String fuelType;

    @ApiModelProperty(notes = "최고출력")
    private String highestOutput;

    @ApiModelProperty(notes = "최대토크")
    private String maximumTorque;

    @ApiModelProperty(notes = "구동방식")
    private String driveMethod;

    @ApiModelProperty(notes = "복합연비")
    private String combinedFuelEconomy;

    @ApiModelProperty(notes = "도심연비")
    private String cityFuelEfficiency;

    @ApiModelProperty(notes = "고속도로연비")
    private String highwayFuelEfficiency;

    @ApiModelProperty(notes = "연비(등급)")
    private String fuelEconomyRating;

    @ApiModelProperty(notes = "공차중량")
    private String toleranceWeight;

    @ApiModelProperty(notes = "앞타이어규격")
    private String frontTireSpecification;

    @ApiModelProperty(notes = "뒷타이어규격")
    private String rearTireSpecification;

    @ApiModelProperty(notes = "저공해등급")
    private String lowPollutionGrade;

    @ApiModelProperty(notes = "최고속도")
    private String topSpeed;

    @ApiModelProperty(notes = "정원")
    private Integer fixedNumber;

    @ApiModelProperty(notes = "이산화탄소배출량")
    private String carbonDioxideEmissions;

    private BurpItem(BurpItemBuilder builder) {
        this.brandName = builder.brandName;
        this.brandImage = builder.brandImage;
        this.carModelId = builder.carModelId;
        this.modelFullName = builder.modelFullName;
        this.external = builder.external;
        this.modelYear = builder.modelYear;
        this.gradeName = builder.gradeName;
        this.burpName = builder.burpName;
        this.price = builder.price;
        this.displacement = builder.displacement;
        this.fuelType = builder.fuelType;
        this.highestOutput = builder.highestOutput;
        this.maximumTorque= builder.maximumTorque;
        this.driveMethod = builder.driveMethod;
        this.combinedFuelEconomy = builder.combinedFuelEconomy;
        this.cityFuelEfficiency = builder.cityFuelEfficiency;
        this.highwayFuelEfficiency = builder.highwayFuelEfficiency;
        this.fuelEconomyRating = builder.fuelEconomyRating;
        this.toleranceWeight = builder.toleranceWeight;
        this.frontTireSpecification = builder.frontTireSpecification;
        this.rearTireSpecification = builder.rearTireSpecification;
        this.lowPollutionGrade = builder.lowPollutionGrade;
        this.topSpeed = builder.topSpeed;
        this.fixedNumber = builder.fixedNumber;
        this.carbonDioxideEmissions = builder.carbonDioxideEmissions;
    }

    public static class BurpItemBuilder implements CommonModelBuilder<BurpItem> {

        private final String brandName;
        private final String brandImage;
        private final Long carModelId;
        private final String modelFullName;
        private final String external;
        private final Integer modelYear;
        private final String gradeName;
        private final String burpName;
        private final String price;
        private final String displacement;
        private final String fuelType;
        private final String highestOutput;
        private final String maximumTorque;
        private final String driveMethod;
        private final String combinedFuelEconomy;
        private final String cityFuelEfficiency;
        private final String highwayFuelEfficiency;
        private final String fuelEconomyRating;
        private final String toleranceWeight;
        private final String frontTireSpecification;
        private final String rearTireSpecification;
        private final String lowPollutionGrade;
        private final String topSpeed;
        private final Integer fixedNumber;
        private final String carbonDioxideEmissions;

        public BurpItemBuilder(Burp burp) {
            this.brandName = burp.getGrade().getCarModel().getCarBrand().getBrandName();
            this.brandImage = burp.getGrade().getCarModel().getCarBrand().getBrandImage();
            this.carModelId = burp.getGrade().getCarModel().getId();
            this.modelFullName = burp.getGrade().getCarModel().getModelName() + " [" + burp.getGrade().getCarModel().getModelType().getName() + "]";
            this.external = burp.getGrade().getCarModel().getExternal();
            this.modelYear = burp.getGrade().getCarModel().getModelYear();
            this.gradeName = burp.getGrade().getGradeName();
            this.burpName = burp.getBurpName();
            this.price = burp.getPrice() + "만원";
            this.displacement = burp.getDisplacement() + "cc";
            this.fuelType = burp.getFuelType().getName();
            this.highestOutput = burp.getHighestOutput() + "ps";
            this.maximumTorque = burp.getMaximumTorque() + "Nm";
            this.driveMethod = burp.getDriveMethod();
            this.combinedFuelEconomy = burp.getCombinedFuelEconomy() + "km/l";
            this.cityFuelEfficiency = burp.getCityFuelEfficiency() + "km/l";
            this.highwayFuelEfficiency = burp.getHighwayFuelEfficiency() + "km/l";
            this.fuelEconomyRating = burp.getFuelEconomyRating() + "등급";
            this.toleranceWeight = burp.getToleranceWeight() + "kg";
            this.frontTireSpecification = burp.getFrontTireSpecification();
            this.rearTireSpecification = burp.getRearTireSpecification();
            this.lowPollutionGrade = burp.getLowPollutionGrade() + "등급";
            this.topSpeed = burp.getTopSpeed() + "km/h";
            this.fixedNumber = burp.getFixedNumber();
            this.carbonDioxideEmissions = burp.getCarbonDioxideEmissions() + "g/km";

        }

        @Override
        public BurpItem build() {
            return new BurpItem(this);
        }
    }
}
