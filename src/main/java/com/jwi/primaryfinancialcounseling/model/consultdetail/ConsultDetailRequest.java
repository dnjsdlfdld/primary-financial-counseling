package com.jwi.primaryfinancialcounseling.model.consultdetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ConsultDetailRequest {
    @NotNull
    @ApiModelProperty(notes = "트림 등록번호", required = true)
    private Long burpId;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "고객명", required = true)
    private String customName;

    @NotNull
    @Length(min = 12, max = 13)
    @ApiModelProperty(notes = "연락처", required = true)
    private String phone;
}
