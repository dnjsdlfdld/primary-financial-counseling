package com.jwi.primaryfinancialcounseling.model.consultdetail;

import com.jwi.primaryfinancialcounseling.entity.ConsultDetail;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultDetailItem {
    @ApiModelProperty(notes = "상담 등록번호")
    private Long id;

    @ApiModelProperty(notes = "고객명")
    private String customName;

    @ApiModelProperty(notes = "연락처")
    private String phone;

    @ApiModelProperty(notes = "상담 등록일자")
    private LocalDateTime applicationTime;

    @ApiModelProperty(notes = "제조사명")
    private String brandName;

    @ApiModelProperty(notes = "제조사 로고")
    private String brandImage;

    @ApiModelProperty(notes = "모델 등록번호")
    private Long carModelId;

    @ApiModelProperty(notes = "모델명")
    private String modelFullName;

    @ApiModelProperty(notes = "외장")
    private String external;

    @ApiModelProperty(notes = "년식")
    private Integer modelYear;

    @ApiModelProperty(notes = "등급명")
    private String gradeName;

    @ApiModelProperty(notes = "트림명")
    private String burpName;

    @ApiModelProperty(notes = "가격")
    private String price;

    @ApiModelProperty(notes = "배기량")
    private String displacement;

    @ApiModelProperty(notes = "연료")
    private String fuelType;

    @ApiModelProperty(notes = "최고출력")
    private String highestOutput;

    @ApiModelProperty(notes = "최대토크")
    private String maximumTorque;

    @ApiModelProperty(notes = "구동방식")
    private String driveMethod;

    @ApiModelProperty(notes = "복합연비")
    private String combinedFuelEconomy;

    @ApiModelProperty(notes = "도심연비")
    private String cityFuelEfficiency;

    @ApiModelProperty(notes = "고속도로연비")
    private String highwayFuelEfficiency;

    @ApiModelProperty(notes = "연비(등급)")
    private String fuelEconomyRating;

    @ApiModelProperty(notes = "공차중량")
    private String toleranceWeight;

    @ApiModelProperty(notes = "앞타이어규격")
    private String frontTireSpecification;

    @ApiModelProperty(notes = "뒷타이어규격")
    private String rearTireSpecification;

    @ApiModelProperty(notes = "저공해등급")
    private String lowPollutionGrade;

    @ApiModelProperty(notes = "최고속도")
    private String topSpeed;

    @ApiModelProperty(notes = "정원")
    private Integer fixedNumber;

    @ApiModelProperty(notes = "이산화탄소배출량")
    private String carbonDioxideEmissions;

    private ConsultDetailItem(ConsultDetailItemBuilder builder) {
        this.id = builder.id;
        this.customName = builder.customName;
        this.phone = builder.phone;
        this.applicationTime = builder.applicationTime;
        this.brandName = builder.brandName;
        this.brandImage = builder.brandImage;
        this.carModelId = builder.carModelId;
        this.modelFullName = builder.modelFullName;
        this.external = builder.external;
        this.modelYear = builder.modelYear;
        this.gradeName = builder.gradeName;
        this.burpName = builder.burpName;
        this.price = builder.price;
        this.displacement = builder.displacement;
        this.fuelType = builder.fuelType;
        this.highestOutput = builder.highestOutput;
        this.maximumTorque= builder.maximumTorque;
        this.driveMethod = builder.driveMethod;
        this.combinedFuelEconomy = builder.combinedFuelEconomy;
        this.cityFuelEfficiency = builder.cityFuelEfficiency;
        this.highwayFuelEfficiency = builder.highwayFuelEfficiency;
        this.fuelEconomyRating = builder.fuelEconomyRating;
        this.toleranceWeight = builder.toleranceWeight;
        this.frontTireSpecification = builder.frontTireSpecification;
        this.rearTireSpecification = builder.rearTireSpecification;
        this.lowPollutionGrade = builder.lowPollutionGrade;
        this.topSpeed = builder.topSpeed;
        this.fixedNumber = builder.fixedNumber;
        this.carbonDioxideEmissions = builder.carbonDioxideEmissions;
    }

    public static class ConsultDetailItemBuilder implements CommonModelBuilder<ConsultDetailItem> {
        private final Long id;
        private final String customName;
        private final String phone;
        private final LocalDateTime applicationTime;
        private final String brandName;
        private final String brandImage;
        private final Long carModelId;
        private final String modelFullName;
        private final String external;
        private final Integer modelYear;
        private final String gradeName;
        private final String burpName;
        private final String price;
        private final String displacement;
        private final String fuelType;
        private final String highestOutput;
        private final String maximumTorque;
        private final String driveMethod;
        private final String combinedFuelEconomy;
        private final String cityFuelEfficiency;
        private final String highwayFuelEfficiency;
        private final String fuelEconomyRating;
        private final String toleranceWeight;
        private final String frontTireSpecification;
        private final String rearTireSpecification;
        private final String lowPollutionGrade;
        private final String topSpeed;
        private final Integer fixedNumber;
        private final String carbonDioxideEmissions;

        public ConsultDetailItemBuilder(ConsultDetail consultDetail) {
            this.id = consultDetail.getId();
            this.customName = consultDetail.getCustomName();
            this.phone = consultDetail.getPhone();
            this.applicationTime = consultDetail.getApplicationTime();
            this.brandName = consultDetail.getBurp().getGrade().getCarModel().getCarBrand().getBrandName();
            this.brandImage = consultDetail.getBurp().getGrade().getCarModel().getCarBrand().getBrandImage();
            this.carModelId = consultDetail.getBurp().getGrade().getCarModel().getId();
            this.modelFullName = consultDetail.getBurp().getGrade().getCarModel().getModelName() + " [" + consultDetail.getBurp().getGrade().getCarModel().getModelType().getName() + "]";
            this.external = consultDetail.getBurp().getGrade().getCarModel().getExternal();
            this.modelYear = consultDetail.getBurp().getGrade().getCarModel().getModelYear();
            this.gradeName = consultDetail.getBurp().getGrade().getGradeName();
            this.burpName = consultDetail.getBurp().getBurpName();
            this.price = consultDetail.getBurp().getPrice() + "만원";
            this.displacement = consultDetail.getBurp().getDisplacement() + "cc";
            this.fuelType = consultDetail.getBurp().getFuelType().getName();
            this.highestOutput = consultDetail.getBurp().getHighestOutput() + "ps";
            this.maximumTorque = consultDetail.getBurp().getMaximumTorque() + "Nm";
            this.driveMethod = consultDetail.getBurp().getDriveMethod();
            this.combinedFuelEconomy = consultDetail.getBurp().getCombinedFuelEconomy() + "km/l";
            this.cityFuelEfficiency = consultDetail.getBurp().getCityFuelEfficiency() + "km/l";
            this.highwayFuelEfficiency = consultDetail.getBurp().getHighwayFuelEfficiency() + "km/l";
            this.fuelEconomyRating = consultDetail.getBurp().getFuelEconomyRating() + "등급";
            this.toleranceWeight = consultDetail.getBurp().getToleranceWeight() + "kg";
            this.frontTireSpecification = consultDetail.getBurp().getFrontTireSpecification();
            this.rearTireSpecification = consultDetail.getBurp().getRearTireSpecification();
            this.lowPollutionGrade = consultDetail.getBurp().getLowPollutionGrade() + "등급";
            this.topSpeed = consultDetail.getBurp().getTopSpeed() + "km/h";
            this.fixedNumber = consultDetail.getBurp().getFixedNumber();
            this.carbonDioxideEmissions = consultDetail.getBurp().getCarbonDioxideEmissions() + "g/km";
        }

        @Override
        public ConsultDetailItem build() {
            return new ConsultDetailItem(this);
        }
    }
}
