package com.jwi.primaryfinancialcounseling.model;

import com.jwi.primaryfinancialcounseling.entity.CarModel;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarInfoDetailResponse { // 값은 Response 에서 채워줌
    private String makeLogoUrl;

    private CarInfoDetailRangeItem priceItem;

    private String carType;

    private List<String> carFuelTypes;

    private CarInfoDetailRangeItem displacementItem;

    private CarInfoDetailRangeItem fuelEfficiencyItem;

    private CarInfoDetailRangeItem personnelCountItem;

    private CarInfoDetailResponse(CarInfoDetailResponseBuilder builder) {
        this.makeLogoUrl = builder.makeLogoUrl;
        this.priceItem = builder.priceItem;
        this.carType = builder.carType;
        this.carFuelTypes = builder.carFuelTypes;
        this.displacementItem = builder.displacementItem;
        this.fuelEfficiencyItem = builder.fuelEfficiencyItem;
        this.personnelCountItem = builder.personnelCountItem;
    }

    public static class CarInfoDetailResponseBuilder implements CommonModelBuilder<CarInfoDetailResponse> {

        private final String makeLogoUrl;
        private final CarInfoDetailRangeItem priceItem;
        private final String carType;
        private final List<String> carFuelTypes; // 가솔린,디젤 콤마 제외하고 리스트만 제공
        private final CarInfoDetailRangeItem displacementItem;
        private final CarInfoDetailRangeItem fuelEfficiencyItem;
        private final CarInfoDetailRangeItem personnelCountItem;

        public CarInfoDetailResponseBuilder(
                CarModel carModel, // 밑 데이터들은 모두 트림에 있지만 모델 조회 시 해당 데이터가 고정되기 때문에 모델 레코드 필요
                Double minPrice,
                Double maxPrice,
                List<String> carFuelTypes,
                Integer minDisplacement,
                Integer maxDisplacement,
                Double minFuelEfficiency,
                Double maxFuelEfficiency,
                Integer minPersonnelCount,
                Integer maxPersonnelCount
        ) {
            this.makeLogoUrl = carModel.getCarBrand().getBrandImage();
            this.carType = carModel.getModelType().getName();

            CarInfoDetailRangeItem priceItem = new CarInfoDetailRangeItem();
            /**
             *Setter 를 사용하여 값을 넣어준다.
             *Setter 를 사용하는 이유는 빌더에서 빌더를 호출하면 복잡하기 때문
              */

            priceItem.setMinDoubleValue(minPrice);
            priceItem.setMaxDoubleValue(maxPrice);
            this.priceItem = priceItem;

            this.carFuelTypes = carFuelTypes;

            CarInfoDetailRangeItem detailRangeItem = new CarInfoDetailRangeItem();
            detailRangeItem.setMinIntValue(minDisplacement);
            detailRangeItem.setMaxIntValue(maxDisplacement);
            this.displacementItem = detailRangeItem;

            CarInfoDetailRangeItem fuelEfficiencyItem = new CarInfoDetailRangeItem();
            fuelEfficiencyItem.setMinDoubleValue(minFuelEfficiency);
            fuelEfficiencyItem.setMinDoubleValue(maxFuelEfficiency);
            this.fuelEfficiencyItem = fuelEfficiencyItem;

            CarInfoDetailRangeItem personnelCountItem = new CarInfoDetailRangeItem();
            personnelCountItem.setMinIntValue(minPersonnelCount);
            personnelCountItem.setMaxIntValue(maxPersonnelCount);
            this.personnelCountItem = personnelCountItem;
        }

        @Override
        public CarInfoDetailResponse build() {
            return new CarInfoDetailResponse(this);
        }
    }
}
