package com.jwi.primaryfinancialcounseling.model;

import com.jwi.primaryfinancialcounseling.entity.CarModel;
import com.jwi.primaryfinancialcounseling.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarListItem {
    private Long modelId;
    private String carFullName;


    private CarListItem(CarListItemBuilder builder) {
        this.carFullName = builder.carFullName;
        this.modelId = builder.modelId;
    }

    public static class CarListItemBuilder implements CommonModelBuilder<CarListItem> {

        private final Long modelId;
        private final String carFullName;

        public CarListItemBuilder(CarModel carModel) {
            this.modelId = carModel.getId();
            this.carFullName = carModel.getCarBrand().getBrandName() + " " + carModel.getModelName();
        }

        @Override
        public CarListItem build() {
            return new CarListItem(this);
        }
    }
}
