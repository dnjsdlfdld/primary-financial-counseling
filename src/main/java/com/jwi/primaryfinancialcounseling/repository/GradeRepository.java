package com.jwi.primaryfinancialcounseling.repository;

import com.jwi.primaryfinancialcounseling.entity.Grade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GradeRepository extends JpaRepository<Grade, Long> {
}
