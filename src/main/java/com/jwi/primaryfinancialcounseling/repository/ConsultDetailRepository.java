package com.jwi.primaryfinancialcounseling.repository;

import com.jwi.primaryfinancialcounseling.entity.ConsultDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultDetailRepository extends JpaRepository<ConsultDetail, Long> {
}
