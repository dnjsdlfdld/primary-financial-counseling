package com.jwi.primaryfinancialcounseling.repository;

import com.jwi.primaryfinancialcounseling.entity.Burp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BurpRepository extends JpaRepository<Burp, Long> {
    List<Burp> findAllByGrade_CarModel_Id(Long carModelId);
    // Burp > carModel 까지 거쳐 올라가기 위해 한 단계식 쿼리 작성
    // Burp 에다가 하는 이유는 데이터 필드들이 트림에 있기 때문
}
