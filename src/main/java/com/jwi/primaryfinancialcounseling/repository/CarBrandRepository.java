package com.jwi.primaryfinancialcounseling.repository;

import com.jwi.primaryfinancialcounseling.entity.CarBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarBrandRepository extends JpaRepository<CarBrand, Long> {
}
