package com.jwi.primaryfinancialcounseling.repository;

import com.jwi.primaryfinancialcounseling.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {
}
