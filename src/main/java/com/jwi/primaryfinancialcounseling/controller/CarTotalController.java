package com.jwi.primaryfinancialcounseling.controller;

import com.jwi.primaryfinancialcounseling.entity.CarBrand;
import com.jwi.primaryfinancialcounseling.entity.CarModel;
import com.jwi.primaryfinancialcounseling.entity.Grade;
import com.jwi.primaryfinancialcounseling.model.CarInfoDetailResponse;
import com.jwi.primaryfinancialcounseling.model.burp.BurpItem;
import com.jwi.primaryfinancialcounseling.model.burp.BurpRequest;
import com.jwi.primaryfinancialcounseling.model.carbrand.CarBrandItem;
import com.jwi.primaryfinancialcounseling.model.carbrand.CarBrandRequest;
import com.jwi.primaryfinancialcounseling.model.carmodel.CarModelItem;
import com.jwi.primaryfinancialcounseling.model.carmodel.CarModelRequest;
import com.jwi.primaryfinancialcounseling.model.etc.CommonResult;
import com.jwi.primaryfinancialcounseling.model.etc.ListResult;
import com.jwi.primaryfinancialcounseling.model.etc.SingleResult;
import com.jwi.primaryfinancialcounseling.model.grade.GradeItem;
import com.jwi.primaryfinancialcounseling.model.grade.GradeRequest;
import com.jwi.primaryfinancialcounseling.service.CarTotalService;
import com.jwi.primaryfinancialcounseling.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 통합 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-total")
public class CarTotalController {
    private final CarTotalService carTotalService;

    @ApiOperation(value = "제조사 등록")
    @PostMapping("/new-brand")
    public CommonResult setCarBrand(@RequestBody @Valid CarBrandRequest request) {
        carTotalService.setCarBrand(request);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "제조사Id", value = "제조사 등록번호", required = true)
    })
    @ApiOperation(value = "제조사 조회")
    @GetMapping("/car-brand/{carBrandId}")
    public SingleResult<CarBrandItem> getCarBrand(@PathVariable long carBrandId) {
        return ResponseService.getSingleResult(carTotalService.getCarBrand(carBrandId));
    }

    @ApiOperation(value = "제조사 리스트 조회")
    @GetMapping("/brand-all")
    public ListResult<CarBrandItem> getCarBrands() {
        return ResponseService.getListResult(carTotalService.getCarBrands(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "제조사Id", value = "제조사 등록번호", required = true)
    })
    @ApiOperation(value = "제조사 수정")
    @PutMapping("/car-brand/{carBrandId}")
    public CommonResult putCarBrand(@PathVariable long carBrandId, @RequestBody @Valid CarBrandRequest request) {
        carTotalService.putCarBrand(carBrandId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 모델 등록")
    @PostMapping("/new-model")
    public CommonResult setCarModel(@RequestBody @Valid CarModelRequest request) {
        CarBrand carBrand = carTotalService.getCarBrandId(request.getCarBrandId());
        carTotalService.setCarModel(carBrand, request);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "모델Id", value = "모델 등록번호", required = true)
    })
    @ApiOperation(value = "차량 모델 조회")
    @GetMapping("/car-model/{carModelId}")
    public SingleResult<CarModelItem> getCarModel(@PathVariable long carModelId) {
        return ResponseService.getSingleResult(carTotalService.getCarModel(carModelId));
    }

    @ApiOperation(value = "모델 리스트 조회")
    @GetMapping("/model-all")
    public ListResult<CarModelItem> getCarModels() {
        return ResponseService.getListResult(carTotalService.getCarModels(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "모델Id", value = "모델 등록번호", required = true)
    })
    @ApiOperation(value = "모델 수정")
    @PutMapping("/car-model/{carModelId}")
    public CommonResult putCarModel(@PathVariable long carModelId, @RequestBody @Valid CarModelRequest request) {
        carTotalService.putCarModel(carModelId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "제조사 변경")
    @PutMapping("/car-model-id/{carModelId}/car-brand-id/{carBrandId}")
    public CommonResult putCarModelId(@PathVariable long carModelId, @PathVariable long carBrandId) {
        CarBrand carBrand = carTotalService.getCarBrandId(carBrandId);
        carTotalService.putCarModelId(carModelId, carBrand);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 등급 등록")
    @PostMapping("/new-grade")
    public CommonResult setGrade(@RequestBody @Valid GradeRequest request) {
        CarModel carModel = carTotalService.getCarModelId(request.getCarModelId());
        carTotalService.setGrade(carModel, request);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "등급Id", value = "등급 등록번호", required = true)
    })
    @ApiOperation(value = "등급 조회")
    @GetMapping("/grade/{gradeId}")
    public SingleResult<GradeItem> getGrade(@PathVariable long gradeId) {
        return ResponseService.getSingleResult(carTotalService.getGrade(gradeId));
    }

    @ApiOperation(value = "등급 리스트 조회")
    @GetMapping("/grade-all")
    public ListResult<GradeItem> getGrades() {
        return ResponseService.getListResult(carTotalService.getGrades(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "등급Id", value = "등급 등록번호", required = true)
    })
    @ApiOperation(value = "등급 수정")
    @PutMapping("/grade/{gradeId}")
    public CommonResult putGrade(@PathVariable long gradeId, @RequestBody @Valid GradeRequest request) {
        carTotalService.putGrade(gradeId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "모델 변경")
    @PutMapping("/grade-id/{gradeId}/car-model-id/{carModelId}")
    public CommonResult putGradeId(@PathVariable long gradeId, @PathVariable long carModelId) {
        CarModel carModel = carTotalService.getCarModelId(carModelId);
        carTotalService.putGradeId(gradeId, carModel);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "트림 등록")
    @PostMapping("/new-burp")
    public CommonResult setBurp(@RequestBody @Valid BurpRequest request) {
        Grade grade = carTotalService.getGradeId(request.getGradeId());
        carTotalService.setBurp(grade, request);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "트림Id", value = "트림 등록번호", required = true)
    })
    @ApiOperation(value = "트림 조회")
    @GetMapping("/burp/{burpId}")
    public SingleResult<BurpItem> getBurp(@PathVariable long burpId) {
        return ResponseService.getSingleResult(carTotalService.getBurp(burpId));
    }

    @ApiOperation(value = "트림 리스트 조회")
    @GetMapping("/burp-all")
    public ListResult<BurpItem> getBurps() {
        return ResponseService.getListResult(carTotalService.getBurps(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "트림Id", value = "트림 등록번호", required = true)
    })
    @ApiOperation(value = "트림 수정")
    @PutMapping("/burp/{burpId}")
    public CommonResult putBurp(@PathVariable long burpId, @RequestBody @Valid BurpRequest request) {
        carTotalService.putBurp(burpId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "등급 레코드수정")
    @PutMapping("/burp-id/{burpId}/grade/{gradeId}")
    public CommonResult putBurpId(@PathVariable long burpId, @PathVariable long gradeId) {
        Grade grade = carTotalService.getGradeId(gradeId);
        carTotalService.putBurpId(burpId, grade);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "모델Id", value = "모델 등록번호", required = true)
    })
    @ApiOperation(value = "차량 디테일 조회")
    @GetMapping("/car-models/{carModelId}")
    public SingleResult<CarInfoDetailResponse> getCarInfoDetail(@PathVariable long carModelId) {

        return ResponseService.getSingleResult(carTotalService.getDetail(carModelId));
    }
}
