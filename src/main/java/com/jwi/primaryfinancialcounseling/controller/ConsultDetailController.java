package com.jwi.primaryfinancialcounseling.controller;

import com.jwi.primaryfinancialcounseling.entity.Burp;
import com.jwi.primaryfinancialcounseling.model.etc.CommonResult;
import com.jwi.primaryfinancialcounseling.model.consultdetail.ConsultDetailRequest;
import com.jwi.primaryfinancialcounseling.service.CarTotalService;
import com.jwi.primaryfinancialcounseling.service.ConsultDetailService;
import com.jwi.primaryfinancialcounseling.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "상담내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/consult-detail")
public class ConsultDetailController {
    private final ConsultDetailService consultDetailService;
    private final CarTotalService carTotalService;

    @ApiOperation(value = "상담내역 등록")
    @PostMapping("/new")
    public CommonResult setConsultDetail(@RequestBody @Valid ConsultDetailRequest request) {
        Burp burp = carTotalService.getBurpId(request.getBurpId());
        consultDetailService.setConsultDetail(burp, request);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "상담Id", value = "상담 등록번호", required = true)
    })
    @ApiOperation(value = "상담내역 수정")
    @PutMapping("/consult-detail/{consultDetailId}")
    public CommonResult putConsultDetail(@PathVariable long consultDetailId, @RequestBody @Valid ConsultDetailRequest request) {
        consultDetailService.putConsultDetail(consultDetailId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "트림 변경")
    @PutMapping("/consult-detail-id/{consultDetailId}/burp-id/{burpId}")
    public CommonResult putConsultDetailId(@PathVariable long consultDetailId, @PathVariable long burpId) {
        Burp burp = carTotalService.getBurpId(burpId);
        consultDetailService.putConsultDetailId(consultDetailId, burp);

        return ResponseService.getSuccessResult();
    }
}
