package com.jwi.primaryfinancialcounseling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimaryFinancialCounselingApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrimaryFinancialCounselingApplication.class, args);
    }

}
